//
//  ViewController.swift
//  Animations
//
//  Created by Ahmed Adm on 24/01/1442 AH.
//  Copyright © 1442 Ahmed Adm. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var transparentView = UIView()
    
    @IBOutlet var popview: UIView!
    @IBOutlet var users_view: UIView!
    @IBOutlet weak var message: UILabel!
    
    
    let height: CGFloat = 500
    var counter = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.popview.bounds = CGRect(x: 0, y: 0, width: self.view.bounds.width-100, height: 20)
        self.popview.frame  = CGRect(x: -10, y: 100, width: self.view.bounds.width-150, height: 20)
        self.popview.alpha = 0.0
        self.view.addSubview(self.popview)
        
    }
    
    @IBAction func open_view(_ sender: Any) {
        
        self.counter += 1
        
        DispatchQueue.global(qos: .userInteractive).sync {
            DispatchQueue.main.async {
                
                self.message.text = "انت رقم \(self.counter)"
                //self.popview.removeFromSuperview()
                //let mainHeight =  self.view.frame.size.height
                
                let left    = CGAffineTransform(translationX: -600, y: 0)
                let right   = CGAffineTransform(translationX: 300, y: 0)
                let bottom  = CGAffineTransform(translationX: 0, y: 300)
                let top     = CGAffineTransform(translationX: 0, y: -300)
                
                //self.popview.center = self.view.center
                //self.popview.bounds = self.view.bounds
                
                //self.popview.transform = right
                
                
                UIView.animate(
                    withDuration: 0.8,
                    delay: 0.0,
                    options: [.curveEaseIn ],
                    animations: {
                        //self.popview.transform = .identity
                        self.popview.alpha = 0.5
                }) { (completed) in
                    
                }
                
                UIView.animate(
                    withDuration: 1.0,
                    delay: 0.8,
                    options: [.curveEaseIn ],
                    animations: {
                        self.popview.alpha = 1.0
                }) { (completed) in
                    
                }
                
                
                UIView.animate(
                    withDuration: 1.0,
                    delay: 1.8,
                    options: [.curveEaseIn , ],
                    animations: {
                        //self.popview.transform = left
                        self.popview.alpha = 0.0
                }) { (completed) in
                    //self.popview.removeFromSuperview()
                }
                
                //                UIViewPropertyAnimator(duration: 1.5, curve: .easeInOut, animations: {
                //                    self.popview.alpha = 0.5
                //                    //self.popview.removeFromSuperview()
                //                }).startAnimation()
                
            }
            
        }
        
    }
    
    @IBAction func open_users(_ sender: Any) {
        
        let window = UIApplication.shared.keyWindow
        transparentView.backgroundColor = UIColor.black.withAlphaComponent(0.9)
        transparentView.frame = self.view.frame
        window?.addSubview(transparentView)
        
        let screenSize = UIScreen.main.bounds.size
        self.users_view.frame = CGRect(x: 0, y: screenSize.height, width: screenSize.width, height: height)
        window?.addSubview(self.users_view)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onClickTransparentView))
        transparentView.addGestureRecognizer(tapGesture)
        
        transparentView.alpha = 0
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
            self.transparentView.alpha = 0.8
            self.users_view.frame = CGRect(x: 0, y: screenSize.height - self.height, width: screenSize.width, height: self.height)
        }, completion: nil)
        
        
    }
    
    
    @objc func onClickTransparentView() {
        let screenSize = UIScreen.main.bounds.size
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
            self.transparentView.alpha = 0
            self.users_view.frame = CGRect(x: 0, y: screenSize.height, width: screenSize.width, height: self.height)
        }, completion: nil)
    }
    
    
}

